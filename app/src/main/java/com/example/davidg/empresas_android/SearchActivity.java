// Nesta Activity possui um campo para busca por nome de empresas.
//O resultado da busca e uma lista de empresa
//Ao clicar em um elemeto da lista uma nova activity é criada.
//A nova  Activity contem informaçoes detalhadas sobre a empresa

package com.example.davidg.empresas_android;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.davidg.empresas_android.api.EnterpriseAPI;
import com.example.davidg.empresas_android.objects.Enterprise;
import com.example.davidg.empresas_android.objects.EnterpriseListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import static android.R.layout.simple_expandable_list_item_1;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {


    private String URL_BASE;
    private String accesstoken;
    private String uid;
    private String client;
    private ListView lvEnterprise;
    /*obs: o prcesso de busca esta sendo iniciado no oncreate para teste
    E posteriormente devera ser ligado ao listener com searchView
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        lvEnterprise = (ListView) findViewById(R.id.lv_enterprise);

        //Recebendo parametros de acesso
        Intent intent = getIntent();
        URL_BASE =  intent.getStringExtra("URL_BASE");
        accesstoken = intent.getStringExtra("access-token");
        uid = intent.getStringExtra("uid");
        client = intent.getStringExtra("client");


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        EnterpriseAPI enterpriseAPI  = retrofit.create(EnterpriseAPI.class);

        //Entrada da busca.
        // Devera receber valor do SearchView
        String enter = " ";


        /*Classificação da entrada da busca
        verifica o primeiro caracter da entra. si for um numero então é  realizado uma busca especifica
        caso a entra seja vazio ele busca todos as empresas
        e caso seja so um nome e exibido uma mesagem pedindo para expecificar o tipo da empresa  */
        retrofit2.Call call = null;

        String[] aux = enter.split(" ");


        if (enter.equals(" ")) {
            //Busca todas as empresas
            call = enterpriseAPI.enterprisesList(accesstoken, client, uid);

        }else if(Character.isDigit(aux[0].charAt(0))){
            //aux[0] tipo de empresa
            //aux[1] nome de empresa

            //Busca elemento especifico
            call = enterpriseAPI.enterprisesIndexwithFilter(accesstoken,client,uid, Integer.parseInt(aux[0]) ,aux[1]);

        }else{
            //orintar usuario a inseri um tipo
            Toast toast = Toast.makeText(SearchActivity.this, "Insira um tipo da empresa mais o nome para a busca Ex: 1 AQM S.A",Toast.LENGTH_SHORT);
            toast.show();

        }

        if(call != null) {
            call.enqueue(new Callback<EnterpriseListResponse>() {


                @Override
                public void onResponse(Call<EnterpriseListResponse> call, Response<EnterpriseListResponse> response) {
                    //Log.d("script", "onResponse: ");

                    //Log.d("script",response.body().enterprises.get(0).toString());

                    final List<Enterprise> list = response.body().enterprises;
                    // Log.d("script","AQUI"+list.toString());

                    //Log.d("script","AQUI"+list.toString());
                    ArrayAdapter<Enterprise> adlist = new ArrayAdapter<Enterprise>(SearchActivity.this, simple_expandable_list_item_1, list);

                    lvEnterprise.setAdapter(adlist);


                    //Adicionar evento a cada elemeto na list view
                    //Add action in listview create a new activit that have dateil of order
                    lvEnterprise.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {

                            // cria a intent
                            Intent intent = new Intent(SearchActivity.this, EnterpriseActivity.class);
                            // Passando um objeto Enterprise
                            intent.putExtra("Enterprise", list.get(position));
                            // chama a Activity que mostra os detalhes
                            startActivity(intent);

                        }

                    });

                }

                @Override
                public void onFailure(Call<EnterpriseListResponse> call, Throwable t) {
                    Log.d("script", "onFailure: " + t.toString());
                    Toast toast = Toast.makeText(SearchActivity.this, "Erro de conexão",Toast.LENGTH_SHORT);
                    toast.show();

                }
            });



        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        Log.d("script", "onQueryTextChange: ");
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(SearchActivity.this);
        return true;

    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("script", "onQueryTextSubmit: ");

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("script", "onQueryTextChange: ");

        return false;
    }
}

