package com.example.davidg.empresas_android.objects;

/**
 * Created by davidg on 11/08/17.
 */


public class User {


    public String email;
    public String password;

    public User(String email, String password){

        this.email = email;
        this.password = password;
    }
   @Override
    public String toString() {
       return "User{"+"email='" + email + '\''  + ",password='" + password + '\'' +'}';

   }
}