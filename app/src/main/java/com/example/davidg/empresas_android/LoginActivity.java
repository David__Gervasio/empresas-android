/*Aplicativo empresa


Nesta Activity e solicitado o email e senha do usuario.
após a confirmação o usuario e direcionado para uma SearchActivity
onde ele podera fazer buscas por empresas

Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;

Servidor: http://54.94.179.135:8090
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

*/
package com.example.davidg.empresas_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.davidg.empresas_android.api.LoginAPI;
import com.example.davidg.empresas_android.objects.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends AppCompatActivity {
    private Button btnEnter;
    private final String  URL_BASE = "http://54.94.179.135:8090/api/v1/";
    private EditText edtEmail;
    private EditText edtPasseword;
    private String email;
    private String passaword;

    Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(URL_BASE)
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Button pausar
        btnEnter = (Button) findViewById(R.id.btn_enter);
        btnEnter.setOnClickListener(pausarColetaOnClickListener);

        //EditTexts
        edtEmail = (EditText) findViewById(R.id.edit_email);
        edtPasseword = (EditText) findViewById(R.id.edit_passeword);


    }
    /*Evento do botão inicia o processo de login.
      Si tudo correr bem o usuario e direcionado para
      para SearchActivity
    */
    private View.OnClickListener pausarColetaOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            //entrada
            email = edtEmail.getText().toString();
            passaword = edtPasseword.getText().toString();

            LoginAPI loginAPI = retrofit.create(LoginAPI.class);
            User user = new User ("testeapple@ioasys.com.br", "12341234" );
            Call<Object> call = loginAPI.sign_in(user );

            call.enqueue(new Callback<Object>( ) {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    Object access = response.body();
                    okhttp3.Headers headers = response.headers();
                    Log.d("script", "onResponse: header " + headers.get("access-token"));


                    //em caso de sucesso iniciar SearchActivity
                    if(response.code() == 200) {
                        // cria a intent
                        Intent intent = new Intent(LoginActivity.this, SearchActivity.class);
                        // seta o parametro
                        intent.putExtra("URL_BASE", URL_BASE);
                        intent.putExtra("access-token", headers.get("access-token"));
                        intent.putExtra("uid", headers.get("uid"));
                        intent.putExtra("client", headers.get("client"));
                        // chama a Activity que mostra os detalhes
                        startActivity(intent);
                    }else{
                        Toast toast = Toast.makeText(LoginActivity.this, "Erro de Autenticação",Toast.LENGTH_SHORT);
                        toast.show();

                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.d("script", "onFailure:"+ t.toString());
                }

            });

        }

    };


}


