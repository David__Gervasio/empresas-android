package com.example.davidg.empresas_android.objects;

import java.util.List;

/**
 * Created by davidg on 11/08/17.
 */

public class EnterpriseListResponse {

    public List<Enterprise> enterprises;
}
