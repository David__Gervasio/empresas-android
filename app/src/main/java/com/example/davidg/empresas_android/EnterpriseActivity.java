package com.example.davidg.empresas_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.example.davidg.empresas_android.api.EnterpriseAPI;
import com.example.davidg.empresas_android.objects.Enterprise;
import com.example.davidg.empresas_android.objects.EnterpriseListResponse;

import java.util.List;

/*import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;*/


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EnterpriseActivity extends AppCompatActivity {


    private TextView txEnterpriseDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);
        txEnterpriseDescription =(TextView)findViewById(R.id.textView);
        Intent intent = getIntent();
        Enterprise enterprise = (Enterprise) intent.getSerializableExtra("Enterprise");
        Log.d("script", "onCreate:   EnterpriseActivity  " + enterprise.toString());


        txEnterpriseDescription.setText(Html.fromHtml("<br />  Origem: " + enterprise.getDescription() +

                " <br />"));
        txEnterpriseDescription.setTextSize(22);


    }
}