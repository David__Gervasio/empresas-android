package com.example.davidg.empresas_android.api;
/*

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
*/


import com.example.davidg.empresas_android.objects.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by davidg on 10/08/17.
 */

public interface LoginAPI {

    @POST("users/auth/sign_in")
    Call< Object> sign_in(@Body User user);

}
