package com.example.davidg.empresas_android.objects;

import java.io.Serializable;

/**
 * Created by davidg on 10/08/17.
 */

public class Enterprise implements Serializable {

    int id;
    String enterprise_name;
    String description;
    String email_enterprise;
    String facebook;
    String twitter;
    String linkedin;
    String phone;
    String own_enterprise;
    String photo;
    int value;
    int shares;
    int share_price;
    int own_shares;
    String city;
    String country;
    Enterprise_type enterprise_type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(String email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(String own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public int getShare_price() {
        return share_price;
    }

    public void setShare_price(int share_price) {
        this.share_price = share_price;
    }

    public int getOwn_shares() {
        return own_shares;
    }

    public void setOwn_shares(int own_shares) {
        this.own_shares = own_shares;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Enterprise_type getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(Enterprise_type enterprise_type) {
        this.enterprise_type = enterprise_type;
    }

    @Override
    public String toString() {
        return  enterprise_name;/* "Enterprise{" +
                "id=" + id +
                ", enterprise_name='" + enterprise_name + '\'' +
                ", description='" + description + '\'' +
                ", email_enterprise='" + email_enterprise + '\'' +
                ", facebook='" + facebook + '\'' +
                ", twitter='" + twitter + '\'' +
                ", linkedin='" + linkedin + '\'' +
                ", phone='" + phone + '\'' +
                ", own_enterprise='" + own_enterprise + '\'' +
                ", photo='" + photo + '\'' +
                ", value=" + value +
                ", shares=" + shares +
                ", share_price=" + share_price +
                ", own_shares=" + own_shares +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", enterprise_type=" + enterprise_type +
                '}';*/
    }
}
