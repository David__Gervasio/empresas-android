package com.example.davidg.empresas_android.api;

import com.example.davidg.empresas_android.objects.Enterprise;
import com.example.davidg.empresas_android.objects.EnterpriseListResponse;

import java.util.List;

/*import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;*/

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
/**
 * Created by davidg on 10/08/17.
 */

public interface EnterpriseAPI {

    //Busca todas as empresas
    @GET("enterprises")
    Call<EnterpriseListResponse>enterprisesList(@Header("access-token") String accessToken, @Header("client") String client, @Header("uid") String uid );


    //Busca uma empresa especifica
    @GET("enterprises?enterprise_types=?&name=")
    Call<EnterpriseListResponse> enterprisesIndexwithFilter(@Header("access-token") String accessToken, @Header("client") String client, @Header("uid") String uid , @Query("enterprise_types") int enterprise_types, @Query("name") String name);




}

